import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'vpn_check_platform_interface.dart';

/// An implementation of [VpnCheckPlatform] that uses method channels.
class MethodChannelVpnCheck extends VpnCheckPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('vpn_check');

  @override
  Future<bool> checkVPN() async {
    final version = await methodChannel.invokeMethod<bool>('checkVPN');
    return version!;
  }
}
