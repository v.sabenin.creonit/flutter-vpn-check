
import 'vpn_check_platform_interface.dart';

class VpnCheck {
  Future<bool> checkVPN() {
    return VpnCheckPlatform.instance.checkVPN();
  }
}
