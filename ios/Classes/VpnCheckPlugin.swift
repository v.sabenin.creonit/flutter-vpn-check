import Flutter
import UIKit

import Foundation
import Network

public class VpnCheckPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "vpn_check", binaryMessenger: registrar.messenger())
    let instance = VpnCheckPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    switch call.method {
    case "checkVPN":
        if #available(iOS 12.0, *) {
            let c = NWConnection(host: "yandex.ru", port: 443, using: .tcp)
            c.stateUpdateHandler = { state in
                if (state == .ready) {
                    if (c.currentPath?.usesInterfaceType(.other) == true) {
                        result(true)
                    } else {
                        result(false)
                    }
                }
            }
            c.start(queue: .main)
        } else {
           result(false)
        }
    default:
      result(FlutterMethodNotImplemented)
    }
  }
}
