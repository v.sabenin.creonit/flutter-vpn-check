import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'vpn_check_method_channel.dart';

abstract class VpnCheckPlatform extends PlatformInterface {
  /// Constructs a VpnCheckPlatform.
  VpnCheckPlatform() : super(token: _token);

  static final Object _token = Object();

  static VpnCheckPlatform _instance = MethodChannelVpnCheck();

  /// The default instance of [VpnCheckPlatform] to use.
  ///
  /// Defaults to [MethodChannelVpnCheck].
  static VpnCheckPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [VpnCheckPlatform] when
  /// they register themselves.
  static set instance(VpnCheckPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<bool> checkVPN() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
