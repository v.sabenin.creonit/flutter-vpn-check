import 'package:flutter_test/flutter_test.dart';
import 'package:vpn_check/vpn_check.dart';
import 'package:vpn_check/vpn_check_platform_interface.dart';
import 'package:vpn_check/vpn_check_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockVpnCheckPlatform
    with MockPlatformInterfaceMixin
    implements VpnCheckPlatform {

  @override
  Future<bool> checkVPN() => Future.value(false);
}

void main() {
  final VpnCheckPlatform initialPlatform = VpnCheckPlatform.instance;

  test('$MethodChannelVpnCheck is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelVpnCheck>());
  });

  test('checkVPN', () async {
    VpnCheck vpnCheckPlugin = VpnCheck();
    MockVpnCheckPlatform fakePlatform = MockVpnCheckPlatform();
    VpnCheckPlatform.instance = fakePlatform;

    //expect(await vpnCheckPlugin.getPlatformVersion(), false);
  });
}
